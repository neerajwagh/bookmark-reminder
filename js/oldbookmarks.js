var timefilter = new Date('2014-01-01') // get this from options page. Somehow.	
var message = []
var notifmap = []
var index = 0

//bootstrap the extension after install
chrome.runtime.onInstalled.addListener(function (){

	chrome.bookmarks.getTree( function( results ){

		tree = results[0].children // array of top level folders
		for (var i = 0; i < tree.length; i++) { 
			recursefolder( tree[i] )// feed each folder to recursefolder 
		}

		chrome.storage.sync.clear(function(){

			chrome.storage.sync.set({ bookmarks : message },function(){	

				if (chrome.runtime.lastError){
					alert('ERROR : ' + chrome.runtime.lastError.message)
				}
				else{
					alert('Bookmarks loaded successfully!')
				}
			})	
		})
	})
})

chrome.runtime.onStartup.addListener(function (){

	chrome.storage.sync.get('bookmarks', function( items ){

		message = items.bookmarks
	})	
})


//Random bookmark on icon click (SURPRISE ME FEATURE)
chrome.browserAction.onClicked.addListener( function() {

	if( ! message ){//fetch message from memory if it is undefined due to event page being taken out of memory
		chrome.storage.sync.get('bookmarks', function( items ){
			var message = items.bookmarks
			var randomindex = Math.floor((Math.random() * message.length));
			chrome.tabs.create({ "url" : message[randomindex].url });
		})	
	}
	else{
	var randomindex = Math.floor((Math.random() * message.length));
	chrome.tabs.create({ "url" : message[randomindex].url });
	}
})


var recursechildren = function( arrayoflinks ){

	for (var j = 0; j < arrayoflinks.length; j++) {
		if( arrayoflinks[j].url ){
			if ( new Date(arrayoflinks[j].dateAdded) < timefilter ){	// bookmarks before timefilter
					//console.log ( arrayoflinks[j].url + '---' + new Date(arrayoflinks[j].dateAdded) )
					message.push({ title : arrayoflinks[j].title, url : arrayoflinks[j].url})
			}	
		}
		else{
			recursefolder( arrayoflinks[j] )
		}
	}
}


var recursefolder = function ( foldernode ) {//takes a folder node in bookmark heirarchy

	recursechildren( foldernode.children )
}


var shownextnotif = function(  ){

	options = {
			"type": "basic",
			"iconUrl": "icons/logo.png",
			"title": "Bookmark Reminder!	",
			"message": message[index].title,
			"isClickable": true
	}

	chrome.notifications.create( options , function( newnotifid ){

		notifmap.push({
			"notifid" : newnotifid,
			"notifindex" : index
		})

		if ( index < message.length ){
			if( index == message.length - 1 ){//all old bookmarks covered. Start again
				index = 0
			}
			else{
				index ++ 
			}				
		}
	})	
}

//listens for new tabs, shows notif on newtab creation.
chrome.tabs.onCreated.addListener( function ( newtab ){

	if ( newtab.url == "chrome://newtab/" ){
		shownextnotif ( )
	}
})


//when notif is clicked, create newtab with bookmark's url and clean previous newtabs.
chrome.notifications.onClicked.addListener( function( notificationId ) {

	for (var i = 0; i < notifmap.length; i++) {

		if ( notifmap[i].notifid == notificationId ){
			var tempindex = notifmap[i].notifindex
			createProperties = {
				"url": message[tempindex].url, //insert URL from bookmarks
				"active" : true,
				"selected" : true
			}

			chrome.tabs.create( createProperties )
			break
		}
	}
	//makes sure there aren't unwanted new tabs crowding the tab bar.
	chrome.tabs.query({url : "chrome://newtab/"}, function(result){
		//get IDs of all opened newtabs and store them in array.
		var blanktabids = []
		for(var i = 0; i < result.length;  i++)
			blanktabids.push(result[i].id)
		chrome.tabs.remove(blanktabids)
	})
})

	

// chrome.storage.onChanged.addListener( function( changes , areaName ) {
        
//           if ( changes["lastindex"].newValue ){

//           		 index = changes["lastindex"].newValue
//           }
//           else{

//           	    index = changes["lastindex"].oldValue
//           }
          
       
//       })





